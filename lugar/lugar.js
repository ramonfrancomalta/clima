const axios = require('axios');

const getLugarLatLong = async(dir) => {
    const encodedUrl = encodeURI(dir);

    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${dir}`,
        headers: { "x-rapidapi-key": "d125cbfd4dmsh08d3b59bf2df4eep15f5c3jsn0d9cb8372df8" }
    });

    const resp = await instance.get()

    if (resp.data.Results.Lenght === 0) {
        throw new Error(`No hay resultados para ${dir}`);
    }

    const data = resp.data.Results[0];
    const direccion = data.name;
    const lat = data.lat;
    const lng = data.lon;


    return {
        direccion,
        lat,
        lng
    }
}


module.exports = {
    getLugarLatLong
}