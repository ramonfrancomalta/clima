const lugar = require('./lugar/lugar');
const clima = require('./clima/clima');

const argv = require('yargs').options({
        direccion: {
            alias: 'd',
            desc: "Nombre de la ciudad para obtener el clima",
            demand: true
        }
    })
    .help()
    .argv;


// lugar.getLugarLatLong(argv.direccion)
//     .then(console.log);

// clima.getClima(40, -74)
//     .then(console.log);

const getInfo = async(direccion) => {

    try {
        const coord = await lugar.getLugarLatLong(direccion);
        const temp = await clima.getClima(coord.lat, coord.lng);
        return (`El clima de ${coord.direccion} es: ${temp}`);
    } catch (e) {
        return (`No se pudo determinar el clima de ${direccion}`);
    }

}

getInfo(argv.direccion)
    .then(console.log)
    .catch(console.log);