const axios = require('axios');

const getClima = async(lat, lon) => {
    const resp = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=59d2e976e3e2e884090fd288e10f239f&units=metric`);

    return resp.data.main.temp;
}

module.exports = {
    getClima
}